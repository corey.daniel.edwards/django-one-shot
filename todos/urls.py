from django.urls import path
from todos.views import (
    TodoListListView,
    TodoListCreateView,
    TodoListDetailView,
    TodoListDeleteView,
    TodoListUpdateView,
    TodoItemCreateView,
    TodoItemUpdateView
)

urlpatterns = [
    path("",TodoListListView.as_view(),name='ListListView'),
    path("<int:pk>/", TodoListDetailView.as_view(), name='ListDetailView'),
    path("create", TodoListCreateView.as_view(), name='ListCreateView'),
    path('<int:pk>/edit/', TodoListUpdateView.as_view(), name='ListUpdateView'),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="ListDeleteView"),
    path("items/create/", TodoItemCreateView.as_view(), name="ItemCreateView"),
    path("items/<int:pk>/edit/", TodoItemUpdateView.as_view(), name="ItemUpdateView")
]
