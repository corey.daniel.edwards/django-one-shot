from django.shortcuts import render
from todos.models import TodoList, TodoItem
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    template_name = 'TodoList/list.html'

class TodoListDetailView(DetailView):
    model = TodoList
    template_name = 'TodoList/detail.html'

class TodoListCreateView(CreateView):
    model = TodoList
    template_name = 'TodoList/create.html'
    fields = ['name']

    def get_success_url(self):
        return reverse_lazy("ListDetailView", args=[self.object.id])

class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = 'TodoList/update.html'
    fields = ['name']

    def get_success_url(self):
        return reverse_lazy("ListUpdateView", args=[self.object.id])

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = 'TodoList/delete.html'
    success_url = reverse_lazy("ListDeleteView")

class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = 'TodoItems/create.html'

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = 'TodoItems/update.html'